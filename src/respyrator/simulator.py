##############################################################################
# For copyright and license notices, see LICENSE file in root directory
##############################################################################
# sudo socat pty,link=/dev/ttyS100,raw,echo=0 PTY,link=/dev/ttyS101,raw,echo=0
import sys
import socket
import time, threading

import numpy as np
from respyrator import core, serial


class Simulator():

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._serial = None
        self.serial_setup()
        self._config_peep = 10
        self._config_pip = 20
        self._config_bpm = 15
        

        threading.Timer(1, self.send_config ).start()


    def send_config(self):
        self.serial_send("CONFIG %d %d %d 0 0 0 0 0 0 0\r\n" % (self._config_pip, self._config_peep, self._config_bpm))
        threading.Timer(3, self.send_config ).start()


    def serial_setup(self):
        file = core.config['serial_file']
        if file:
            self.serial = serial.FileSerial(file)
            return
        port = core.config['serial_port']
        if not port and core.debug:
            core.logger.debug(
                'In debug mode connect to fakeSerial, for force port add '
                '"serial_port" in config.yml')
            self.serial = serial.FakeSerial()
            return
        if not port:
            ports = serial.serial_ports_get()
            port = serial.serial_discovery_port(ports, quick=True)
        if not port:
            print(
                'You must set a "serial_port" value for config file '
                'config.yml')
            sys.exit(-1)
        core.logger.debug('Connect to port "%s"' % port)
        self.serial = serial.serial_get(port)
        if not self.serial.is_open:
            raise Exception('Can\'t open serial port %s' % port)
        self.serial.reset_input_buffer()

    def serial_send(self, msg):
        core.logger.info('Serial send "%s"' % msg)
        self.serial.write(bytes('%s\r\n' % msg, 'utf8'))
        self.serial.flush()

    def serial_read(self):
        pass


def app():
    main = Simulator()

# vim: tabstop=4 ai expandtab shiftwidth=4 softtabstop=4
