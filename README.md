# resPyRator
Python software for the respeerator.

Python3

# Instalacion

Clonar la rama y instalar las dependencias

```bash
pip3 install -r requirements.txt
```

# respyratorctl
Con este comando se controla la interfaz.

Iniciar la UI con el dispositivo conectado...
```bash
respyratorctl ui
```

Si no hay dispositivo conectado, usar el comando
```bash
respyratorctl ui --debug
```
