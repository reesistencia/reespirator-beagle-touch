#!/bin/bash
#gpio1_17 for arduino reset pin, pull high to turn on
#GPIOb_p = b * 32 + p = N

#enable the pin
echo 49 > /sys/class/gpio/export
sleep 0.1
#set for output
echo "out" > /sys/class/gpio/gpio49/direction
sleep 0.1
#set HIGH
#(echo 0 > /sys/class/gpio/gpio49/value && sleep 0.9 && echo 1 > /sys/class/gpio/gpio49/value) &
echo 0 > /sys/class/gpio/gpio49/value && sleep 0.9
/bin/echo 1 > /sys/class/gpio/gpio49/value && avrdude -c stk500v2 -p m2560 -v -v -P /dev/ttyO1 -D -U flash:w:$1
